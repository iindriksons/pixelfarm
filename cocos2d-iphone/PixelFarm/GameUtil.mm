//
// GameUtil.m
// TenFloorsDown
//
// Created by Thomas Bruno on 4/10/12.
//
// Copyright (c) 2012 Thomas Bruno
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//



#import "GameUtil.h"

@implementation GameUtil

+(CGPoint)tileCoordForPosition:(CGPoint)position withMap:(CCTMXTiledMap*)tileMap {
	CGPoint scaledPosition = ccp(position.x * CC_CONTENT_SCALE_FACTOR(),position.y * CC_CONTENT_SCALE_FACTOR());
	int x = scaledPosition.x / tileMap.tileSize.width;
    int y = ((tileMap.mapSize.height * tileMap.tileSize.height) - (position.y * CC_CONTENT_SCALE_FACTOR()) ) / tileMap.tileSize.height;
	
    return ccp(x, y);
}

+(CGPoint)positionForTileCoord:(CGPoint)tileCoord withMap:(CCTMXTiledMap*)tileMap {
	CGSize scaledSize = CGSizeMake(tileMap.tileSize.width / CC_CONTENT_SCALE_FACTOR(), tileMap.tileSize.height / CC_CONTENT_SCALE_FACTOR());
	float x = tileCoord.x * scaledSize.width;
	
	float y = abs((tileCoord.y + 1) - tileMap.mapSize.height);
	y = y * (scaledSize.height);
		
	return ccp(x,y);
}

+(BOOL)isPlanterTool:(ToolTypes)tool {
	bool rtn = false;
	
	switch(tool) {
		case kToolPlantCorn:
		case kToolPlantBroccoli:
		case kToolPlantCucumber:
		case kToolPlantTomato:
		case kToolPlantPotato:
		case kToolPlantRedPepper:
		case kToolPlantCarrot:
			rtn = true;
			break;
		default:
			rtn = false;
			break;
	}
	
	return rtn;
}


+(BOOL)tileCheck:(int)tileGID toolType:(ToolTypes)tool {
	BOOL rtnVal = false;
	
	switch(tool) {
		case kToolHoe:
			switch (tileGID) {
				case kTileDirt:
				case kTileGrass:
				case kTileGrassLineE:
				case kTileGrassLineN:
				case kTileGrassLineW:
				case kTileGrassLineS:
				case kTileGrassLineNE:
				case kTileGrassLineNW:
				case kTileGrassLineSE:
				case kTileGrassLineSW:
					rtnVal = true;
					break;
				default:
					rtnVal = false;
					break;
			}
			break;
		case kToolPlantCorn:
		case kToolPlantBroccoli:
		case kToolPlantCucumber:
		case kToolPlantTomato:
		case kToolPlantPotato:
		case kToolPlantRedPepper:
		case kToolPlantCarrot:
			switch (tileGID) {
				case kTilePlowed:
					rtnVal = true;
					break;
				default:
					rtnVal = false;
					break;
			}
			break;
		case kToolHarvest:
			rtnVal = false;
			break;
		default:
			rtnVal = false;
			break;
	}
	
	return rtnVal;
}

@end
