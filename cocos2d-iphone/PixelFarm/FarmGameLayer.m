//
// FarmGameLayer.m
// PixelFarm
//
// Created by Thomas Bruno on 7/28/12.
//
// Copyright (c) 2012 Thomas Bruno
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//


#import "cocos2d.h"
#import "GameUtil.h"
#import "FarmGameLayer.h"
#import "Plant.h"
#import "GameUtil.h"

@implementation FarmGameLayer

- (id)init
{
    self = [super init];
    if (self) {
		tileMap = [CCTMXTiledMap tiledMapWithTMXFile:@"BaseTileMap.tmx"];
		gridLayer = [tileMap layerNamed:@"Grid"];
		playLayer = [tileMap layerNamed:@"Play"];

		plants = [[NSMutableArray alloc] init];
		
		[self addChild:tileMap z:-1];
		
    }
    return self;
}

-(void)useTool:(ToolTypes)tool at:(CGPoint)position {
	
	CGPoint tileLoc = [GameUtil tileCoordForPosition:position withMap:tileMap];
	int GID = [playLayer tileGIDAt:tileLoc];
	
	if([GameUtil tileCheck:GID toolType:tool]) {
		CGPoint rTileLoc = [GameUtil positionForTileCoord:tileLoc withMap:tileMap];

		if(tool == kToolPlantBroccoli || tool == kToolPlantCarrot || tool == kToolPlantCorn || tool == kToolPlantCucumber || tool == kToolPlantPotato || tool == kToolPlantRedPepper || tool == kToolPlantTomato) {
			[self usePlantTool:tool at:rTileLoc];
		}
	}
}

-(void)usePlantTool:(ToolTypes)plantTool at:(CGPoint)loc {
	
	Plant *plant = nil;
	int centerMod = (tileMap.tileSize.width/CC_CONTENT_SCALE_FACTOR()) * 0.5f;

	switch(plantTool) {
		case kToolPlantCorn:
			plant = [Plant plantWithPlantType:kPlantCorn at:ccp(loc.x + centerMod, loc.y) withParent:self];
			break;
		case kToolPlantBroccoli:
			plant = [Plant plantWithPlantType:kPlantBroccoli at:ccp(loc.x + centerMod, loc.y) withParent:self];
			break;
		case kToolPlantCucumber:
			plant = [Plant plantWithPlantType:kPlantCucumber at:ccp(loc.x + centerMod, loc.y) withParent:self];
			break;
		case kToolPlantTomato:
			plant = [Plant plantWithPlantType:kPlantTomato at:ccp(loc.x + centerMod, loc.y) withParent:self];
			break;
		case kToolPlantPotato:
			plant = [Plant plantWithPlantType:kPlantPotato at:ccp(loc.x + centerMod, loc.y) withParent:self];
			break;
		case kToolPlantRedPepper:
			plant = [Plant plantWithPlantType:kPlantRedPepper at:ccp(loc.x + centerMod, loc.y) withParent:self];
			break;
		case kToolPlantCarrot:
			plant = [Plant plantWithPlantType:kPlantCarrot at:ccp(loc.x + centerMod, loc.y) withParent:self];
			break;
	}
	
	// Add each new plant to the plant array
	if(plant != nil) {
		[plants addObject:plant];
	}
}

-(Plant*)plantInTile:(CGPoint)tilePoint {
	Plant *plant = nil;
	
	CGPoint realPoint = [GameUtil positionForTileCoord:tilePoint withMap:tileMap];
	CGPoint positionPoint = ccp(realPoint.x + ((tileMap.tileSize.width/CC_CONTENT_SCALE_FACTOR()) * 0.5f),realPoint.y + ((tileMap.tileSize.width/CC_CONTENT_SCALE_FACTOR()) * 0.5f));
	
	for (Plant* p in plants) {
		CGRect box = [p boundingBox];
		
		if(CGRectContainsPoint([p boundingBox], positionPoint)) {
			plant = p;
		}
	}
	
   return plant;
}
	
-(BOOL)checkTool:(ToolTypes)tool at:(CGPoint)position {
	bool canUse = false;
	CGPoint tileP = [GameUtil tileCoordForPosition:position withMap:tileMap];
	int GID = [playLayer tileGIDAt:tileP];

	// First check if the tool can even be used on this type of tile
	if([GameUtil tileCheck:GID toolType:tool]) {
		
		// If the tool is a Planter tool, check for other plants
		if([GameUtil isPlanterTool:tool]) {
			if([self plantInTile:tileP] == nil)
				canUse = true;
			else
				canUse = false;
		} else {
			canUse = true;
		}
	}

	return canUse;
}


-(void) drawRect:(CGRect)rect
{
    // The rect is drawn using four lines
    CGPoint pos1, pos2, pos3, pos4;
    pos1 = CGPointMake(rect.origin.x, rect.origin.y);
    pos2 = CGPointMake(rect.origin.x, rect.origin.y + rect.size.height);
    pos3 = CGPointMake(rect.origin.x + rect.size.width,
					   rect.origin.y + rect.size.height);
    pos4 = CGPointMake(rect.origin.x + rect.size.width, rect.origin.y);
	
    ccDrawLine(pos1, pos2);
    ccDrawLine(pos2, pos3);
    ccDrawLine(pos3, pos4);
    ccDrawLine(pos4, pos1);
}



-(void) drawLine:(CGPoint)source to:(CGPoint)dest
{
    // The rect is drawn using four lines
    ccDrawLine(source, dest);
}

@end
