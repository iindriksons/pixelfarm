//
//  GameHudLayer.h
//  PixelFarm
//
//  Created by Thomas Bruno on 8/8/12.
//  Copyright (c) 2012 NaveOSS. All rights reserved.
//
#import "cocos2d.h"
#import "Constants.h"
#import "CCLayer.h"

@class GameLayer;
@class ToolCursor;

@interface GameHudLayer : CCLayer {
	__weak GameLayer *gameLayer;
	ToolCursor *cursorTool;
	
	ToolTypes currentTool;
	PlantTypes currentSeed;
	
	NSMutableArray *seedArray;
	
	BOOL gridActive;
	
}

@property (weak,nonatomic) GameLayer *gameLayer;

-(id)initGameHudWithGameLayer:(GameLayer*)gl;


@end
