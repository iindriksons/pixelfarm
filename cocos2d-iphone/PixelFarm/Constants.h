//
// Constants.h
// PixelFarm
//
// Created by Thomas Bruno on 7/28/12.
//
// Copyright (c) 2012 Thomas Bruno
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#ifndef PixelFarm_Constants_h
#define PixelFarm_Constants_h

#define kTilePlowed 680
#define kTileGrass 866
#define kTileDirt 743
#define kTileGrassLineN 648
#define kTileGrassLineW 679
#define kTileGrassLineE 681
#define kTileGrassLineS 712
#define kTileGrassLineNW 647
#define kTileGrassLineNE 649
#define kTileGrassLineSE 713
#define kTileGrassLineSW 711

typedef enum { kSceneUninitialized, kSceneMainMenu, kSceneTutorial, kSceneFarm, kSceneGameOver, kSceneCredits } GameScenes;
typedef enum { kMusicSuguriWaltz } MusicTracks;
typedef enum { kSound1 } SoundEffects;

typedef enum { kPlantNone, kPlantCucumber, kPlantCorn, kPlantRedPepper, kPlantPotato, kPlantTomato, kPlantBroccoli, kPlantCarrot } PlantTypes;
typedef enum { kPlantStagePlanted, kPlantStageGrowing, kPlantStageHarvested } PlantStages;
typedef enum { kToolNone, kToolBuy, kToolSell, kToolHoe, kToolHarvest, kToolPlantCucumber, kToolPlantCorn, kToolPlantRedPepper, kToolPlantPotato, kToolPlantTomato, kToolPlantBroccoli, kToolPlantCarrot } ToolTypes;

#endif
