//
//  ToolCursor.h
//  PixelFarm
//
//  Created by Thomas Bruno on 8/10/12.
//  Copyright (c) 2012 NaveOSS. All rights reserved.
//
#import "cocos2d.h"

#import "CCSprite.h"
#import "Constants.h"

@interface ToolCursor : CCSprite {
	CCSprite *toolIcon;
	ToolTypes currentTool;
	
	BOOL isUsable;
}

@property (readonly) BOOL isUsable;

-(void)setCursorIndicator:(BOOL)usable;

-(void)setCurrentTool:(ToolTypes)currentTool;
-(ToolTypes)currentTool;



@end
