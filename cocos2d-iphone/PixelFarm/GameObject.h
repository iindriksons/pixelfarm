//
//  GameObject.h
//  PixelFarm
//
//  Created by Thomas Bruno on 7/31/12.
//  Copyright (c) 2012 NaveOSS. All rights reserved.
//

#import "CCSprite.h"

@interface GameObject : CCSprite

@end
