//
//  GameHudLayer.m
//  PixelFarm
//
//  Created by Thomas Bruno on 8/8/12.
//  Copyright (c) 2012 NaveOSS. All rights reserved.
//

#import "GameHudLayer.h"
#import "GameLayer.h"
#import "ToolCursor.h"

@implementation GameHudLayer

@synthesize gameLayer;

#define SquareUsable 1000
#define SquareUnUsable 1001
#define SeedButtonCorn 1002
#define SeedButtonCarrot 1003
#define SeedButtonCucumber 1004
#define SeedButtonBroccoli 1005
#define SeedButtonPotato 1006
#define SeedButtonRedPepper 1007
#define SeedButtonTomato 1008
#define SquareUsableIcon 1009

-(id)initGameHudWithGameLayer:(GameLayer *)gl {
	self = [super init];
	if(self != nil) {
		self.isTouchEnabled = YES;
		
		gridActive = false;
		gameLayer = gl;
		
		cursorTool = [[ToolCursor alloc] init];
		[self addChild:cursorTool];
		
		[cursorTool setVisible:false];
		[cursorTool setCurrentTool:kToolNone];
		[cursorTool setCursorIndicator:false];
				
		[self setupPlantButtons];
		
	}
	return self;
}


-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	for(UITouch *touch in touches) {
		CGPoint loc = [touch locationInView:[touch view]];
		loc = [[CCDirector sharedDirector] convertToGL:loc];
		
		[self setCursorTool:loc];
		[cursorTool setPosition:loc];
		[cursorTool setVisible:true];
	}
}


-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	for(UITouch *touch in touches) {
		CGPoint loc = [touch locationInView:[touch view]];
		loc = [[CCDirector sharedDirector] convertToGL:loc];
		
		if([gameLayer checkTool:[cursorTool currentTool] at:loc]) {
			[cursorTool setCursorIndicator:true];
			[cursorTool setPosition:ccp(loc.x,loc.y + 16)];
		} else {
			[cursorTool setCursorIndicator:false];
			[cursorTool setPosition:ccp(loc.x,loc.y + 16)];		}
	}
}


-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	for(UITouch *touch in touches) {
		CGPoint loc = [touch locationInView:[touch view]];
		loc = [[CCDirector sharedDirector] convertToGL:loc];
		
		if([gameLayer checkTool:[cursorTool currentTool] at:loc]) {
			[gameLayer useTool:[cursorTool currentTool] at:loc];
		}

		[cursorTool setVisible:false];
		[cursorTool setCurrentTool:kToolNone];
	}
}

-(void)setupPlantButtons {
	[self addChild:[CCSprite spriteWithSpriteFrameName:@"CarrotSeedButtonUp.png"] z:0 tag:SeedButtonCarrot];
	[self addChild:[CCSprite spriteWithSpriteFrameName:@"CornSeedButtonUp.png"] z:0 tag:SeedButtonCorn];
	[self addChild:[CCSprite spriteWithSpriteFrameName:@"BroccoliSeedButtonUp.png"] z:0 tag:SeedButtonBroccoli];
	[self addChild:[CCSprite spriteWithSpriteFrameName:@"PotatoSeedButtonUp.png"] z:0 tag:SeedButtonPotato];
	[self addChild:[CCSprite spriteWithSpriteFrameName:@"RedPepperSeedButtonUp.png"] z:0 tag:SeedButtonRedPepper];
	[self addChild:[CCSprite spriteWithSpriteFrameName:@"CucumberSeedButtonUp.png"] z:0 tag:SeedButtonCucumber];
	[self addChild:[CCSprite spriteWithSpriteFrameName:@"TomatoSeedButtonUp.png"] z:0 tag:SeedButtonTomato];
	
	[[self getChildByTag:SeedButtonCarrot] setPosition:ccp(1*48,23*32)];
	[[self getChildByTag:SeedButtonCorn] setPosition:ccp(2*48,23*32)];
	[[self getChildByTag:SeedButtonBroccoli] setPosition:ccp(3*48,23*32)];
	[[self getChildByTag:SeedButtonPotato] setPosition:ccp(4*48,23*32)];
	[[self getChildByTag:SeedButtonRedPepper] setPosition:ccp(5*48,23*32)];
	[[self getChildByTag:SeedButtonCucumber] setPosition:ccp(6*48,23*32)];
	[[self getChildByTag:SeedButtonTomato] setPosition:ccp(7*48,23*32)];
	
	[[(CCSprite*)[self getChildByTag:SeedButtonCarrot] texture] setAliasTexParameters];
	[[(CCSprite*)[self getChildByTag:SeedButtonCorn] texture] setAliasTexParameters];
	[[(CCSprite*)[self getChildByTag:SeedButtonBroccoli] texture] setAliasTexParameters];
	[[(CCSprite*)[self getChildByTag:SeedButtonPotato] texture] setAliasTexParameters];
	[[(CCSprite*)[self getChildByTag:SeedButtonRedPepper] texture] setAliasTexParameters];
	[[(CCSprite*)[self getChildByTag:SeedButtonCucumber] texture] setAliasTexParameters];
	[[(CCSprite*)[self getChildByTag:SeedButtonTomato] texture] setAliasTexParameters];
	
	[[self getChildByTag:SeedButtonCarrot] setScale:1.25f];
	[[self getChildByTag:SeedButtonCorn] setScale:1.25f];
	[[self getChildByTag:SeedButtonBroccoli] setScale:1.25f];
	[[self getChildByTag:SeedButtonPotato] setScale:1.25f];
	[[self getChildByTag:SeedButtonRedPepper] setScale:1.25f];
	[[self getChildByTag:SeedButtonCucumber] setScale:1.25f];
	[[self getChildByTag:SeedButtonTomato] setScale:1.25f];
}

-(void)setCursorTool:(CGPoint)loc {
	
	if(CGRectContainsPoint([[self getChildByTag:SeedButtonCarrot] boundingBox],loc)) {
		[cursorTool setCurrentTool:kToolPlantCarrot];
	} else if (CGRectContainsPoint([[self getChildByTag:SeedButtonCorn] boundingBox],loc)) {
		[cursorTool setCurrentTool:kToolPlantCorn];
	} else if (CGRectContainsPoint([[self getChildByTag:SeedButtonBroccoli] boundingBox],loc)) {
		[cursorTool setCurrentTool:kToolPlantBroccoli];
	} else if (CGRectContainsPoint([[self getChildByTag:SeedButtonPotato] boundingBox],loc)) {
		[cursorTool setCurrentTool:kToolPlantPotato];
	} else if (CGRectContainsPoint([[self getChildByTag:SeedButtonRedPepper] boundingBox],loc)) {
		[cursorTool setCurrentTool:kToolPlantRedPepper];
	} else if (CGRectContainsPoint([[self getChildByTag:SeedButtonCucumber] boundingBox],loc)) {
		[cursorTool setCurrentTool:kToolPlantCucumber];
	} else if (CGRectContainsPoint([[self getChildByTag:SeedButtonTomato] boundingBox],loc)) {
		[cursorTool setCurrentTool:kToolPlantTomato];
	} else {
		[cursorTool setCurrentTool:kToolNone];
	}
}

@end
